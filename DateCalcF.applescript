-- vim: set ft=applescript:

-- HELPERS

-- date in String format to Date
to calendarDate(oldDate)
    set newDate to the current date
    if (text 1 thru 4 of oldDate as integer) < 2100 and (text 1 thru 4 of oldDate as integer) > 2000 then
        set the year of newDate to (text 1 thru 4 of oldDate)
    else
        return false
    end if
    if (text 6 thru 7 of oldDate as integer) < 13 and (text 6 thru 7 of oldDate as integer) > 0 then
        set the month of newDate to (text 6 thru 7 of oldDate)
    else
        return false
    end if
    if (text 9 thru 10 of oldDate as integer) < 32 and (text 9 thru 10 of oldDate as integer) > 0 then
        set the day of newDate to (text 9 thru 10 of oldDate)
    else
        return false
    end if
    set the time of newDate to 0
    return newDate
end calendarDate

-- date in Date format to String
to stringDate(oldDate)
    set {year:y, month:m, day:d} to oldDate

    set y_s to y as string
    if m < 10 then
        set m_s to "0" & (m as integer)
    else
        set m_s to m as integer
    end if
    if d < 10 then
        set d_s to "0" & (d as integer)
    else
        set d_s to d as integer
    end if
    return y_s & "-" & m_s & "-" & d_s
end stringDate

-- MAIN

set curDate to stringDate(current date)

repeat

    -- get user data
    repeat
        set myDate to false
        set yourDate to display dialog "Enter the date:" default answer curDate with icon note buttons {"Continue"} default button "Continue"
        set yourDateText to text returned of yourDate
        -- emergency exit
        if yourDateText is "quit" or yourDateText is "q" then
            error number -128
        end if
        -- check if a proper date entered
        try
            set myDate to calendarDate(yourDateText)
        end try
        if myDate is not false then
            exit repeat
        end if
    end repeat
    repeat
        set myDelta to false
        set yourDelta to display dialog "Enter the delta:" default answer "50" with icon note buttons {"Continue"} default button "Continue"
        set yourDeltaText to text returned of yourDelta
        -- emergency exit
        if yourDeltaText is "quit" or yourDeltaText is "q" then
            error number -128
        end if
        -- check if a proper Delta entered (and also check if it's a positive or a negative one)
        if text 1 thru 1 of yourDeltaText is "-" then
            set myDeltaDirection to "-1" as integer
            try
                set myDelta to text 2 thru -1 of yourDeltaText as integer
            end try
        else
            set myDeltaDirection to "1" as integer
            try
                set myDelta to yourDeltaText as integer
            end try
        end if
        if myDelta is not false then
            exit repeat
        end if
    end repeat

    -- calculate and convert the provided data to a string
    set rezDate to stringDate(myDate + (myDeltaDirection * myDelta * days))

    -- print out the result
    display dialog rezDate with icon note buttons {"Cancel", "OK"} default button "OK"

end repeat
