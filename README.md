# DateCalcF

## About

DateCalcF is a little Apple script application, which serves the sole purpose of adding and subtracting a provided number of days to or from the provided date (default is today). The compiled application can be found [here](https://bitbucket.org/dsjkvf/applescript-datecalcf/downloads/), while `DateCalcF.applescript` file in the repository is a plain text source file.

## Usage

![screenshot](http://i.imgur.com/MHmF5KQ.png)

Just run the [compiled app](https://bitbucket.org/dsjkvf/applescript-datecalcf/downloads/DateCalcF.app.zip).

### Quitting

Either press `Cancel` after performing the calculations, or just type `quit` or  `q` in any of the app's input fields.
